# demo

## Prerequistis
- MAVEN
- Jdk17 or Jdk19

- See the wiki for detailed instructions for Mac Os and Windows
  - https://gitlab.com/arjanassink1/restassured-demo/-/wikis/home

## Run Backend application
- open terminal and type mvn clean install
- then go to src/main/java and start Application.java main class in folder restbackend
- or run from command-line in directory restbackend "mvn spring-boot:run"

## Run backend in Docker with docker-compose
- open terminal and make sure you are in the folder /restbackend
- run from terminal 'mvn package spring-boot:repackage'
- run from terminal 'docker-compose up'  (add -d for detached mode)
  - to stop run from terminal docker-compose down

# Backend Swagger and Database console
You can use:
- swagger-url: http://localhost:8080/swagger-ui.html
- h2-console: http://localhost:8080/h2-console
    - login with username: sa and password: empty
    - jdbc:h2:mem:testdb

# H2-Database

-The H2 database is running in memory, so if you restart the backend then the database will go back to default state
-default there are 3 user inside the database:
* admin, password admin1234, role admin and role user
* user, password user1234, role user
* moderator password moderator1234, role moderator

# Generate open-api.yaml file
- go to maven, select  restbackend
- select plugins 
- select spring-doc-openapi,
- run springdoc-openapi:generate
- a file openapi.yaml is generated in the target/openapispec folder
- make sure the application is started, otherwise it doesnt work (connection refused error)

[comment]: <> (# frontend)

[comment]: <> (- go to terminal to folder frontend)

[comment]: <> (- eerste keer run npm install)

[comment]: <> (- run ng serve --o to start the frontend)

[comment]: <> (- go to http://localhost:4200)
