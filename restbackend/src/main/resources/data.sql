
-- insert default roles
INSERT INTO roles(name) VALUES('ROLE_USER');
INSERT INTO roles(name) VALUES('ROLE_MODERATOR');
INSERT INTO roles(name) VALUES('ROLE_ADMIN');

-- insert default users
INSERT INTO users(email,password,username,name) VALUES('admin@test.nl','$2a$10$svng46i/76Kt0tG37Bg39eYljIa1SVmV2QG0nTe9FdvYdF94I8vGG','admin','admin');
INSERT INTO users(email,password,username,name) VALUES('user@test.nl','$2a$10$DkmJzTXLhxexyJjouEosIu5/2rhET1lOcdfvRUziPE16XaDRtdqEK','user','user');
INSERT INTO users(email,password,username,name) VALUES('moderator@test.nl','$2a$10$TO0T8k2q5K3/Vse3LsUfnu0rq5.0M088HZh4phisR3kIDecaFEUDK','moderator','moderator');

--set roles for users
INSERT INTO USER_ROLES(user_id,role_id) VALUES(1,3);
INSERT INTO USER_ROLES(user_id,role_id) VALUES(1,1);
INSERT INTO USER_ROLES(user_id,role_id) VALUES(2,1);
INSERT INTO USER_ROLES(user_id,role_id) VALUES(3,2);

-- insert adresses

INSERT INTO ADRESSES (city,country,description,house_number,street,zip_code) VALUES('Enschede','Netherlands','HomeAddress',8,'Emmastraat','7543RC');
INSERT INTO ADRESSES (city,country,description,house_number,street,zip_code) VALUES('Enschede','Netherlands','WorkAdres',12,'Boulevard','7521BB');
INSERT INTO ADRESSES (city,country,description,house_number,street,zip_code) VALUES('Utrecht','Netherlands','HomeAddress',1,'Aalscholverhof','5382AC');
INSERT INTO ADRESSES (city,country,description,house_number,street,zip_code) VALUES('Utrecht','Netherlands','WorkAddress',10,'Roerdompstraat ','5382AA');
INSERT INTO ADRESSES (city,country,description,house_number,street,zip_code) VALUES('Almere','Netherlands','HomeAddress',5,'Augustuspad','3523KL');
INSERT INTO ADRESSES (city,country,description,house_number,street,zip_code) VALUES('Almere','Netherlands','WorkAddress',5,'A D van Eckstraat','3523KK');
INSERT INTO ADRESSES (city,country,description,house_number,street,zip_code) VALUES('Madrid','Spain','PostAdress',5,'Paseo de la Castellana','28046');

-- set adress for users
INSERT INTO USER_ADRESSES(user_id,adresses_id) VALUES(1,1);
INSERT INTO USER_ADRESSES(user_id,adresses_id) VALUES(1,2);
INSERT INTO USER_ADRESSES(user_id,adresses_id) VALUES(2,3);
INSERT INTO USER_ADRESSES(user_id,adresses_id) VALUES(2,4);
INSERT INTO USER_ADRESSES(user_id,adresses_id) VALUES(3,5);
INSERT INTO USER_ADRESSES(user_id,adresses_id) VALUES(3,6);
INSERT INTO USER_ADRESSES(user_id,adresses_id) VALUES(3,7);
