package io.techchamps.restbackend.controller;

import io.techchamps.restbackend.config.Constants;
import io.techchamps.restbackend.entity.Adresses;
import io.techchamps.restbackend.entity.Role;
import io.techchamps.restbackend.entity.RoleName;
import io.techchamps.restbackend.entity.User;
import io.techchamps.restbackend.exception.NotFoundException;
import io.techchamps.restbackend.repository.AdressesRepository;
import io.techchamps.restbackend.repository.RoleRepository;
import io.techchamps.restbackend.request.AdressRequest;
import io.techchamps.restbackend.request.UserRequest;
import io.techchamps.restbackend.response.UserResponse;
import io.techchamps.restbackend.services.UserServices;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.*;

@RestController
@RequestMapping("/api")
public class UserApi {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    UserServices userServices;

    @Autowired
    AdressesRepository adressesRepository;
    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    private final Constants constants = new Constants();
    private static final String ROLE_NOT_FOUND ="ERROR: Role not found";

    @GetMapping(value = "/users")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<UserResponse> getAllUsers() {

        List<User> listofUser = userServices.getAllUsers();
        List<UserResponse> userResponseList = new ArrayList<>();
        for (User user : listofUser) {
            userResponseList.add(modelMapper.map(user, UserResponse.class));
        }

        return userResponseList;
    }


    @GetMapping(value = "/users/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<UserResponse> getuserById(@PathVariable("id") @Min(1) int id) {
        if (userServices.findById(id).isPresent()) {
            Optional<User> user = userServices.findById(id);
            UserResponse userResponse = modelMapper.map(user,UserResponse.class);
            return ResponseEntity.ok().body(userResponse);
        } else {
            throw new NotFoundException("User with " + id + " not found!");
        }
    }

    @GetMapping(value = "/users/email")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<UserResponse>  getuserByEmail(@RequestParam(value = "email") String email) {

        if (userServices.findByEmail(email).isPresent()) {
            Optional<User> user = userServices.findByEmail(email);
            UserResponse userResponse = modelMapper.map(user,UserResponse.class);
            return ResponseEntity.ok().body(userResponse);
        } else {
           throw new NotFoundException("User with " + email + " not found!");
        }

    }


    @PostMapping(value = "/users")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<UserResponse> addUser(@Valid @RequestBody UserRequest userRequest) {

        //Convert roles from string to Role Enum first
        Set<Role> roles = new HashSet<>();
        Set<String> strRoles = userRequest.getRoles();
        if (strRoles == null) {
            Role userRole = roleRepository.findByName(RoleName.ROLE_USER).orElseThrow(() -> new NotFoundException(ROLE_NOT_FOUND));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin","ADMIN" -> {
                        Role adminrole = roleRepository.findByName(RoleName.ROLE_ADMIN).orElseThrow(() -> new NotFoundException(ROLE_NOT_FOUND));
                        roles.add(adminrole);
                    }
                    case "pm","MODERATOR","moderator" -> {
                        Role modRole = roleRepository.findByName(RoleName.ROLE_MODERATOR)
                                .orElseThrow(() -> new NotFoundException("Error: Role is not found."));
                        roles.add(modRole);

                    }
                    case "user","USER" -> {
                        Role adminrole = roleRepository.findByName(RoleName.ROLE_USER).orElseThrow(() -> new NotFoundException(ROLE_NOT_FOUND));
                        roles.add(adminrole);
                    }
                    default -> {
                        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                                .orElseThrow(() -> new NotFoundException("Error: Role is not found."));
                        roles.add(userRole);
                    }


                }
            });
        }
            User user = modelMapper.map(userRequest, User.class);
            for (AdressRequest adressRequest : userRequest.getAdresses()) {
                Adresses adresses = modelMapper.map(adressRequest, Adresses.class);
                adressesRepository.save(adresses);
            }
            user.setRoles(roles);
            user.setPassword(encoder.encode(constants.getDefaultPassword()));
            userServices.save(user);
            UserResponse userResponse = modelMapper.map(user, UserResponse.class);
            // get id of the saved user and put in into responseobject
            Optional<User> user1 = userServices.findByUsername((user.getUsername()));
            user1.ifPresent(value -> userResponse.setId(value.getId()));
            return ResponseEntity.ok().body(userResponse);
        }


        @DeleteMapping("/users/{id}")
        @PreAuthorize("hasRole('ADMIN')")
        public ResponseEntity<UserResponse>  deleteEmployee ( @PathVariable int id){

            if (userServices.findById(id).isPresent()) {
                Optional<User> user = userServices.findById(id);
                UserResponse userResponse = modelMapper.map(user,UserResponse.class);
                userServices.deleteById(id);
                return ResponseEntity.ok().body(userResponse);
            } else {
                throw new NotFoundException("id niet gevonden");
            }
        }
    }

