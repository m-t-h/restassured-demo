package io.techchamps.restbackend.response;


import io.techchamps.restbackend.request.AdressRequest;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
public class UserResponse {
    private int id;
    private String name;
    private String username;
    private String email;
    private Set<String> roles;
    private List<AdressRequest> adresses;

}
