Feature: SignIn feature


  Scenario: 1. User can log in when credentials are correct
    #1. Implementeer de code voor de Given stap in de SignUpSteps class
    Given my credentials are "admin" and "admin1234"
    When I try to log in
    #2. Implementeer de code voor de Then stap in de SignUpSteps class
    Then the responsecode is 200

  Scenario: 2. User cannot log in when credentials are incorrect
    Given my credentials are "wronguser" and "wrongpassword"
    When I try to log in
    #2. Implementeer de code voor de Then stap in de SignUpSteps class
    Then the responsecode is 401

   Scenario Outline: 3. Combineer bovenstaande tot een scenario Outline

     Examples:
