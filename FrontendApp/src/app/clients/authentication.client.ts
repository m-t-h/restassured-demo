import { environment } from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root',
})

export class AuthenticationClient {
  constructor(private http: HttpClient) {}

  login(username: string, password: string): Observable<any> {
    return this.http.post(environment.apiUrl + '/auth/signin', {
      username,
      password
    }, httpOptions);
  }
  register(name: string,username: string, email: string, password: string): Observable<any> {
    return this.http.post(environment.apiUrl + '/auth/signup', {
      name,
      username,
      email,
      password,
    }, httpOptions);
  }

}

