# Opdrachten 

## Algemeen
- Start de REST backend (run Application onder restbackend/scr/main/java/Application.java, zie ook de README.md voor meer info)
  - Als de backend gestart is kun je de Swagger UI openen via http://localhost:8080/swagger-ui.html.
  - Gebruik Swagger UI bijvoorbeeld te kijken welke response er terug komt
- In deze workshop maken we gebruik vanendpoints met authorisatie en zonder autorisatie.
  - Endpoints zonder authorisatie zijn no-auth-api en auth-api 
  - Endpoints met authorisatie zijn user-api en file-api
- Als je vragen hebt dan kun je ons uiteraard aanspreken
- Handige links om te gebruiken:
  - https://github.com/rest-assured/rest-assured/wiki/Usage 
  - https://javadoc.io/doc/io.rest-assured/rest-assured/latest/index.html
  - https://hamcrest.org/JavaHamcrest/javadoc/1.3/org/hamcrest/Matchers.html

## Opdracht 1
### Opdracht 1a
- Ga naar de map opdrachten in de module workshop en open Opdracht1.java 
  - Maak de methode verder af zodat je een get request doet op http://localhost:8080/no_auth/users
  - Controleer dat de statuscode van de response 200 is

### Opdracht 1b
- Kopieer of verrijk de testmethode van opdracht 1a met de volgende acties:
  - Valideer dat gebruiker 1 de admin is (valideer minimaal de velden id, name, username)
  - Valideer dat er 3 gebruikers in het antwoord zitten (tip: Gebruik de hasItems of hasSize assertion)
  - Valideer dat de naam van de 2e gebruiker 'user' is

### Opdracht 1c
- Maak nogmaals een nieuwe testmethode (bijv. public void valideerAdres) aan in Opdracht1.java en voer de volgende acties uit:
  - Valideer dat de admin gebruiker 2 adressen heeft (tip: Gebruik de hasItems of hasSize assertion)
  - Valideer een paar velden uit de addressen van de admin

### Opdracht 1d
- Maak nu een testmethode waarbij je een request stuurt naar het endpoint "/no_auth/users/email" om een gebruiker op te halen op basis van een emailadres
  - Gebruik een queryParameter als onderdeel van de given().
  - Valideer dat de juiste user terug komt

## Opdracht 1e (optioneel)
- Verrijk op pas je uitwerking van opdracht 1c met een extract van het volledige adres:
  - Sla dit adres op in een HashMap<String,String>> adressesUserAdmin variabele
  - Valideer deze vervolgens (bijv. met Assertions.equals("...", "...") of een hamcrest variant assertThat("....", equalTo("...))
  
## Opdracht 2
### Opdracht 2a
- Maak in de map opdrachten een nieuwe java klasse aan genaamd Opdracht2.java en voer de volgende acties uit
  - Maak een post request op /api/auth/signin en log in met de user admin en wachtwoord admin1234
  - Controleer dat de statuscode 200 is (gebruik assertions van restassured)
  - Optioneel: Maak ook een test waarbij je valideert dat je met ongeldige credentials niet in kunt loggen
- #### Tip gebruik eerst swagger/open-api om te kijken hoe het request verstuurd moet worden (http://localhost:8080/swagger-ui.html)

### Opdracht 2b
- Doe hetzelfde als in de voorgaande opdracht, maar gebruik nu de jsonfile onder resource/jsonfiles als body
  - Lees de json file als volgt uit: File jsonRequest = new File(getClass().getClassLoader().getResource("jsonfiles/loginRequest.json").getFile());)
  - Valideer of de response een token bevat (notNull) en een type (bearer) bevat

### Opdracht 2c
- Maak een nieuwe test en sla deze keer het gehele antwoord met extract op in een Response variable (Response response = given()...)
  - Gebruik voor de request body nogmaals de loginRequest.json
  - Gebruik nu het Response object om verschillende assertions op de statuscode, token en type uit te voeren
    - Voorbeeld: 'String token = response.path("token")' geeft je het token terug om vervolgens op te kunnen valideren.
    - Assertions kun je vervolgens doen met junit of hamcrest assertions. Voorbeelden:
      - junit -> Assertions.equals("...", "...") ;
      - hamcrest -> assertThat("....", equalTo("...));
      
## Opdracht 3 - Authenticatie
- Maak in de map opdrachten een nieuwe java klasse aan genaamd Opdracht3.java
  - Maak een request waarmee je alle users op haalt (/users)
    - Om dit te kunnen doen moet je een valide token meegeven in je request! (gebruik hier oath2 authenticatie, zie ook de REST-Assured Usage Guide)
      - De opdracht is hier dus om eerst in te loggen op /auth/signin) en het token uit de response op te slaan (gebruik extract) 
      - Vervolgens geef je deze mee in het GET-request op gebruikers op te halen (/users)
  - Controleer vervolgens dat je een lijst met users terug krijgt met daarin de user admin, user en moderator.

## Opdracht 4 - Maak en verwijder user en gebruik RequestSpecification (Optioneel)
We gaan in deze opdracht een gebruiker toevoegen door middel van een RequestSpecification. Dit is een herbruikbaar stukje request dat je toe kunt voegen aan de given() van je test
  - Maak nu in de map opdrachten een nieuwe java class aan genaamd Opdracht4.java en voeg de volgende methode toe:
```````
private RequestSpecification createUserRequest() {
        // Schrijf hier de code om een requestSpecification terug te geven. Zie ook presentatie voor inspiratie.
        // Bedenk hoe je hier zowel de authenticatie als de body voor het aanmaken van de user in kwijt kunt
        }
```````
- Maak een nieuwe user aan (postrequest naar /users) en geef deze de rollen admin en user mee en minimaal 1 adres:
  - Gebruik in deze test de createUserRequest() methode in de given -> bijv. given().spec(createUserRequest())
  - Controleer dat de responsecode '200' is.
  - Haal uit de response het id op van de zojuist aangemaakte User en sla deze op voor verder gebruik
  - Verwijder de user door middel van een DELETE request. Bekijk in Swagger UI welk endpoint hiervoor geschikt is

#### Let op: Een user kan maar 1 keer bestaan in de database. Als je een user toevoegt dien je deze daarna dus te verwijderen voordat je een nieuwe POST request doet met dezelfde user gegevens.### 
In de opdracht gebeurt dit middels de DELETE request aan het einde of begin van de test.
Mocht je tegen issues aanlopen, herstart dan de backend zodat de Database weer in een schone uitgangssituatie opstart. 
Of gebruik onderstaande query om een user te verwijderen o.b.v. een id.
```````
Voorbeeld query:
delete from user_roles where USER_ID=4;
delete from user_adresses where USER_ID=4;
delete from users where id=4;
```````

## Opdracht 5 - Object Mapping
Met deze opdrachten gaan we gebruik maken van Object Mapping. Hierbij gaan we JSON
files omzetten naar een Object, en andersom.
- Maak wederom een post request op /api/auth/signin en log in met de user admin en wachtwoord admin1234, maar deze keer op basis van objecten
  - Maak een klasse JwtResponse in 'workshop/src/test/java/io/techchamps/workshop/dto' om de JSON response naar een object om te zetten. Om je op weg te helpen hebben we voor het request al 'LoginRequest' klasse aangemaakt.
    - Maak in de test een nieuw LoginRequest object en geef deze mee in je request als body.
    - Map de response van de signin request op de JwtResponse klasse en print het token
    - Voer een aantal validaties uit op het JwtResponse object

## Opdracht 6 - Automatisch genereren van klasses voor object mapping op basis van openapi.yaml
In deze opdracht gaan we opdracht 5 herhalen maar kun je automatisch de benodige klasses laten genereren voor object mapping.
- Uncomment in de pom.xml (van workshop) het stuk code onder Genereer dtos op basis van openapi.yaml
- Open de terminal en zorg dat je in de map workshop staat en voor het commando 'mvn clean install' uit
  - In de target folder vindt je nu de automatische gegenereerde dtos /target/generated-resources/openapi/src/generated/java/main/generated.dtos/
  - Gebruik deze dto's om je test te maken. 

## Opdracht 7 - Generieke methodes maken voor ophalen token, postrequest, getrequest
In deze opdracht gaan we gebruik maken van de klasse RequestHelper om diverse generieke hulpmethodes te schrijven
- Maak in de Requesthelper een methode genereerToken (LoginRequest loginrequest) die het token teruggeeft
- Maak in de Requesthelper een methode postRequest(Object object, String endpoint) die een Response object teruggeeft
- Maak in de Requesthelper een methode deleteRequest(String endpoint) die een Response object teruggeeft
- Gebruik deze methodes om opdracht 4 te herzien 
- Geef in je request 2 rollen en 2 adressen mee
- Valideer of de rollen in de response overeenkomen met je request
- Valideer of de adressen in de response overeenkomen met je request

## Opdracht 8 Cucumber
- Open onder de resource map signup.feature en probeer de stappen te implementeren
- Ik heb hier alles van RequestHelper gezet in BasicSteps en de postrequest is aangepast met boolean useAuthentication